package main

import "github.com/gin-gonic/gin"
import (
	"./templates"
	"github.com/satori/go.uuid"
	"net/http"
	"sort"
	"time"
)

type timeSlice []templates.Person

func (p timeSlice) Len() int {
	return len(p)
}

func (p timeSlice) Less(i, j int) bool {
	return p[i].CreateTime.After(p[j].CreateTime)
}

func (p timeSlice) Swap(i, j int) {
	p[i], p[j] = p[j], p[i]
}

func main() {
	r := gin.Default()
	persons := make(map[string]templates.Person)

	r.GET("/persons", func(c *gin.Context) {

		render(persons, c)

	})

	r.POST("/persons", func(c *gin.Context) {

		first := c.PostForm("first")
		last := c.PostForm("last")

		person, exists := persons[first]

		if !exists {
			u1 := uuid.Must(uuid.NewV4())
			person = templates.Person{Id: u1.String(), FirstName: first, LastName: last, CreateTime: time.Now()}
		} else {
			person.LastName = last
		}
		persons[first] = person

		render(persons, c)

	})

	r.Run() // listen and serve on 0.0.0.0:8080
}

func render(persons map[string]templates.Person, c *gin.Context) {
	c.Writer.WriteHeader(http.StatusOK)

	sortedPersons := make(timeSlice, 0, len(persons))

	for _, d := range persons {
		sortedPersons = append(sortedPersons, d)
	}

	sort.Sort(sortedPersons)

	response := templates.NameForm() + templates.NamesList(sortedPersons)

	c.Data(200, "text/html; charset=utf-8", []byte(response))
}
